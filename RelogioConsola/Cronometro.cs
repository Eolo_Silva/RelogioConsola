﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RelogioConsola
{
    class Cronometro : Timer
    {
        private DateTime _inicio;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intervalo">Em milisegundos</param>
        public Cronometro(double intervalo)
        {
            base.Interval = intervalo;
            this.Elapsed += TempoZero;
        }

        public void Iniciar()
        {
            this._inicio = DateTime.Now;
            this.Start();
        }

        public TimeSpan Parar()
        {
            this.Stop();
            TimeSpan Transcorrido = DateTime.Now - this._inicio;
            return Transcorrido;
        }

        private void TempoZero(object sender, ElapsedEventArgs e)
        {
            Console.Clear();
            TimeSpan Transcorrido = DateTime.Now - this._inicio;
            Console.WriteLine(Transcorrido.ToString());
        }
    }
}

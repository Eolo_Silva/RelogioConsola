﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RelogioConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            double intervalo = 1;
            Cronometro cronometro = new Cronometro(intervalo);

            cronometro.Iniciar();

            Console.ReadKey();

            TimeSpan final = cronometro.Parar();

            Console.WriteLine("Transcorridos: {0}", final.ToString());
            Console.ReadKey();
        }
    }
}
